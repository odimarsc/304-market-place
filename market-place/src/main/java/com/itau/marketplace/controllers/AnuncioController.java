package com.itau.marketplace.controllers;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Anuncio;
import com.itau.marketplace.repositories.AnuncioRepository;

@RestController
@RequestMapping("/anuncio1")
public class AnuncioController {
	
	@Autowired
	AnuncioRepository anuncioRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Anuncio criar (@RequestBody Anuncio anuncio) {
		
		anuncio.setId(UUID.randomUUID());
		anuncio.setData(LocalDate.now());
		
		return anuncioRepository.save(anuncio);
	}
}
