package com.itau.marketplace.controllers;

import java.time.LocalDate;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Oferta;
import com.itau.marketplace.repositories.OfertaRepository;

@RestController
@RequestMapping("/oferta1")
public class OfertaController {
	
	@Autowired
	OfertaRepository ofertaRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Oferta criar (@RequestBody Oferta oferta) {
		
		oferta.setId(UUID.randomUUID());
		
		return ofertaRepository.save(oferta);
	}
	
}
