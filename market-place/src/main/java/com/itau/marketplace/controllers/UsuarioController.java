package com.itau.marketplace.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Usuario;
import com.itau.marketplace.repositories.UsuarioRepository;

@RestController
@RequestMapping("/usuario1")
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(method=RequestMethod.POST)
	public Usuario criar (@RequestBody Usuario usuario) {
		
		usuario.setId(UUID.randomUUID());
		
		return usuarioRepository.save(usuario);
	}

}
