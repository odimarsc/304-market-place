package com.itau.marketplace.models;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Currency;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Anuncio {
	
	@PrimaryKey
	private UUID id;
	@NotNull
	private String titulo;
	@NotNull
	private String descricao;
	@NotNull
	private double preco;
	@NotNull
	private LocalDate data;
	@NotNull
	private UUID usuario_id;
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public UUID getUsuario_id() {
		return usuario_id;
	}
	public void setUsuario_id(UUID usuario_id) {
		this.usuario_id = usuario_id;
	}
	
}
