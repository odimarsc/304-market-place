package com.itau.marketplace.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Oferta {
	
	@PrimaryKey
	private UUID id;
	@NotNull
	private String texto;
	@NotNull
	private double preco;
	@NotNull
	private UUID anuncio_id;
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public double getPreco() {
		return preco;
	}
	public void setPreco(double preco) {
		this.preco = preco;
	}
	public UUID getAnuncio_id() {
		return anuncio_id;
	}
	public void setAnuncio_id(UUID anuncio_id) {
		this.anuncio_id = anuncio_id;
	}	

}
