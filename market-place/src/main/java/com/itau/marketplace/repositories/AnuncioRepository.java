package com.itau.marketplace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.itau.marketplace.models.Anuncio;

@RepositoryRestResource(collectionResourceRel="anuncio", path="anuncio")
public interface AnuncioRepository extends CrudRepository<Anuncio,UUID> {

}