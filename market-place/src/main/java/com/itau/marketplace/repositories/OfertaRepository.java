package com.itau.marketplace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.itau.marketplace.models.Oferta;

@RepositoryRestResource(collectionResourceRel="oferta", path="oferta")
public interface OfertaRepository extends CrudRepository<Oferta, UUID> {

}
