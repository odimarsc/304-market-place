package com.itau.marketplace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.itau.marketplace.models.Usuario;

@RepositoryRestResource(collectionResourceRel="usuario", path="usuario")
public interface UsuarioRepository extends CrudRepository<Usuario,UUID> {

}
